import argparse
import os
import string
import logging
import shutil
from collections import defaultdict

try:
    import cPickle as pickle
except ImportError:
    import pickle

from settings import \
    PROGRAM_NAME, \
    NO_EXTENSION_NAME, \
    INDEX_FILENAME, \
    INDEX_DAT_FILENAME, \
    MAIN_INDEX_FILENAME


logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def parse_args():
    """Parses arguments for file sorter"""

    def _parse_args(args):
        ignored, groups = [], []
        if args.ignored is not None:
            ignored = args.ignored
        if args.groups is not None:
            groups = string.join(args.groups)
            groups = groups.replace(' ', '.')
            groups = groups.replace('].[', '/')
            groups = groups.replace('[', '/')
            groups = groups.replace(']', '/')
            groups = groups.strip('/')
            groups = groups.split('/')
            groups = [g.split('.') for g in groups]
        return ignored, groups, args.index, args.no_extension
        
    parser = argparse.ArgumentParser(prog=PROGRAM_NAME)
    parser.add_argument('--ignore-extensions', nargs='+', default=None, dest='ignored',
                        help='Specifies which extensions to ignored during sorting')
    parser.add_argument('--groups', nargs='+', default=None, dest='groups',
                        help='Specifies which extensions are to group together')
    parser.add_argument('--no-index', action='store_false', default=True, dest='index',
                        help='Specifies if the index is not to be built')
    parser.add_argument('--no-extension-name', default=NO_EXTENSION_NAME, dest='no_extension',
                        help='Specifies the name of the folder for files with no extension')

    args = parser.parse_args()
    return _parse_args(args)


def create_directories_index(current_dir, groups, ignored, no_extension):
    """Creates a dictionary that maps each extension to its directory and list of files

    `current_dir` - the current directory with is going to be sorted
    `groups` - the extensions to be grouped together into a single directory
    `ignored` - the extensions to ignore
    `no_extension` - the name of the folder where the files without extension will be stored

    """
    def extension(file_name):
        s =  string.split(file_name, '.', 1)
        if len(s) == 1:
            return no_extension
        return s[-1]

    files = [f for f in os.listdir('.') if os.path.isfile(f) and not f.startswith('.')]
    directories = [(g, os.path.join(current_dir, string.join(g).replace(' ', '-')))
                   for g in groups]

    dir_index = dict()
    for extensions, directory in directories:
        for e in extensions:
            dir_index[e] = {'dir': directory, 'files': []}
    for f in files:
        file_extension = extension(f)
        if file_extension not in ignored:
            if file_extension not in dir_index:
                directory = os.path.join(current_dir, file_extension)
                dir_index[file_extension] = {'dir': directory, 'files': []}
            dir_index[file_extension]['files'].append(f)
    return dir_index

        
def create_directories(dir_index):
    """Create the necessary folders

    `dir_index` - the dictionary containing the path of the folder to create for each extension
                  and the corresponding list of files

    """
    for extension, dir_files in dir_index.iteritems():
        directory = dir_files['dir']
        if os.path.isdir(directory):
            continue
        try:
            os.mkdir(directory)
        except OSError:
            logger.error("Something went wrong during creation of directory %s.", directory)


def move_files(current_dir, dir_index, has_index):
    """Moves the files from the current directory to the new directory

    `current_dir` - the current directory
    `dir_index` - the dictionary containing the path of the folder to create for each extension
                  and the corresponding list of files
    `has_index` - the flag that indicates if a textual index has to be written

    """
    for extension, dir_files in dir_index.iteritems():
        directory = dir_files['dir']
        logger.info("Moving file with extension %s in directory %s...", extension,
                    directory)
        for f in dir_files['files']:
            logger.info("Moving file %s...", f)
            src = os.path.join(current_dir, f)
            dst = os.path.join(directory, f)
            try:
                shutil.move(src, dst)
            except (shutil.Error, IOError):
                logger.error("Something went wrong while moving file %s.", f)


def create_indexes(current_dir, dir_index):
    """Creates the textual and the data index

    `current_dir` - the current directory
    `dir_index` - the dictionary containing the path of the folder to create for each extension
                  and the corresponding list of files

    """
    def index_exists(directory):
        return os.path.exists(os.path.join(directory, INDEX_DAT_FILENAME))

    def get_previous_index():
        pass

    directory_files = defaultdict(list)
    for _, dir_files in dir_index.iteritems():
        directory_files[dir_files['dir']].extend(dir_files['files'])

    for directory, files in directory_files.iteritems():
        create_text_index(directory, files)
        dump_json_dat_index(directory, files)
    
    create_main_index(current_dir, directory_files)


def create_text_index(directory, files):
    """Creates the textual index

    `directory` - the directory for which to create the index
    `files` -  the files that are to be indexed

    """
    with open(os.path.join(directory, INDEX_FILENAME), 'w') as fout:
        for index, filename in enumerate(sorted(files)):
            fout.write(str(index + 1) + '. ' + filename + '\n')


def dump_json_dat_index(directory, files):
    """Dumps the data index

    `directory` - the directory for which to create the index
    `files` -  the files that are to be indexed

    """
    with open(os.path.join(directory, INDEX_DAT_FILENAME), 'w') as fout:
        pickle.dump(files, fout, -1)


def create_main_index(current_dir, directory_files):
    """Dumps the data for the main index

    `current_dir` - the current directory
    `directory_files` - the dictionary that maps each directory to its list of files
    """
    with open(os.path.join(current_dir, MAIN_INDEX_FILENAME), 'w') as fout:
        pickle.dump(directory_files, fout, -1)


def main():
    ignored, groups, has_index, no_extension = parse_args()
    current_dir = os.getcwd()
    dir_index = create_directories_index(current_dir, groups, ignored, no_extension)
    create_directories(dir_index)
    move_files(current_dir, dir_index, has_index)
    create_indexes(current_dir, dir_index)


main()
