VERSION = '0.1'
PROGRAM_NAME = 'File Sorter v' + VERSION
NO_EXTENSION_NAME = 'no_extension'

MAIN_INDEX_FILENAME = '.file_sorter.index'
INDEX_FILENAME = 'index.txt'
INDEX_DAT_FILENAME = '.index.dat.json'
